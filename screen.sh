#!/bin/sh
sleep 1
Monitors=$(xrandr | awk '/ connected/ {print $1}')
MainMonitor=$(echo $Monitors | tr " " "\n" | head -n 1)
SecondaryMonitors=$(echo $Monitors | tr " " "\n" | tail -n +2)
xrandr --auto
Last=$MainMonitor
for x in $(echo $SecondaryMonitors | tr " " "\n")
do
  xrandr --output $x --auto --left-of $Last
  Last=$x
done
