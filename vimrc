set runtimepath+=$HOME/.vim"
execute pathogen#infect()
set tabstop=2
set softtabstop=2
set shiftwidth=2
set expandtab
set nosmartindent
set cinkeys-=0#
set indentkeys-=0#
syntax on
filetype plugin indent on
set wildmenu
set incsearch
set hlsearch
set ff=unix
set ffs=unix,dos
set ruler
set nowrap
set backspace=indent,eol,start
set cinoptions=l1
set encoding=utf-8
set fileencoding=utf-8
set formatoptions-=o
set formatoptions-=r
set formatoptions-=cro
set noswapfile
autocmd BufNewFile,BufRead *.frag,*.vert,*.fp,*.vp,*.glsl setf glsl
autocmd BufNewFile,BufRead *.c,*.cpp,*.h,*.hpp syntax keyword cType u8 u16 u32 u64 s8 s16 s32 s64 memsize ptrdiff uptrdiff
colorscheme fruity

if $TERM == "xterm-256color"
  set t_Co=256
endif

if has("win32")
set guifont=DejaVu_Sans_Mono:h9:cANSI
else
set guifont=DejaVu\ Sans\ Mono\ 9
endif

set guioptions-=m  "remove menu bar
set guioptions-=T  "remove toolbar
set guioptions-=r  "remove right-hand scroll bar
set guioptions-=L  "remove left-hand scroll bar
set undofile
set undodir=$HOME/.vim/undo
set undolevels=1000
set undoreload=10000
let g:ycm_global_ycm_extra_conf = $HOME . '/.vim/.ycm_extra_conf.py'
let g:ycm_always_populate_location_list = 1

"maps
map <F5> :GundoToggle <CR>
map <F4> :YcmDiag <CR>
map <C-]> :YcmCompleter GoTo <CR>
map <C-}> :YcmCompleter GoToDeclaration <CR>
map <F3> :make <CR>
"switch buffers easier
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>
vnorem // y/<c-r>"<cr>

noremap  <C-X> <Esc>
noremap! <C-X> <Esc>

"make visual replacements not fuck everything up
vnoremap p "0p
vnoremap P "0P
"vnoremap y "0y
vnoremap d "0d
vnoremap x "0x
